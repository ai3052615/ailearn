from sklearn.decomposition import PCA
from sklearn.datasets import load_iris
import matplotlib.pyplot as plt

# 加载数据集
iris = load_iris()
X = iris.data
y = iris.target

# 创建PCA对象，并指定降维后的维度
pca = PCA(n_components=2)

# 在训练数据上进行PCA降维
X_reduced = pca.fit_transform(X)

# 可视化降维后的数据
plt.scatter(X_reduced[:, 0], X_reduced[:, 1], c=y, cmap='viridis')
plt.xlabel('PC1')
plt.ylabel('PC2')
plt.title('PCA on Iris dataset')
plt.show()