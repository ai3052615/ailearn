# coding:gbk
####  数据集划分
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_squared_error, r2_score

# 加载CSV数据集
data = pd.read_csv('dataset.csv',encoding='gbk')

# 假设'转发量'、'流量指数'、'品类热度'是特征列，'成交量'是目标列
features = data[['转发量', '流量指数', '品类热度']]
target = data['成交量']

# 将数据集按比例划分为训练集和测试集，比如80%的数据用于训练，20%用于测试
# 这里random_state参数是为了保证每次划分结果的一致性，不设置的话每次运行会产生不同的划分结果
X_train, X_test, y_train, y_test = train_test_split(features, target, test_size=0.2, random_state=42)

# 训练模型（这里以线性回归为例）
model = LinearRegression()
model.fit(X_train, y_train)

# 预测
y_pred_train = model.predict(X_train)  # 对训练集进行预测
y_pred_test = model.predict(X_test)  # 对测试集进行预测

# 评估模型
mse_train = mean_squared_error(y_train, y_pred_train)  # 计算训练集上的均方误差
r2_train = r2_score(y_train, y_pred_train)  # 计算训练集上的决定系数R?

mse_test = mean_squared_error(y_test, y_pred_test)  # 计算测试集上的均方误差
r2_test = r2_score(y_test, y_pred_test)  # 计算测试集上的决定系数R?

print(f"训练集均方误差 (MSE): {mse_train}")
print(f"训练集决定系数 R2: {r2_train}")
print("\n")
print(f"测试集均方误差 (MSE): {mse_test}")
print(f"测试集决定系数 R2: {r2_test}")