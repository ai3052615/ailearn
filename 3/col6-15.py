import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt

# 加载数据
data = pd.read_csv('./breast+cancer+wisconsin+diagnostic/wdbc.data', header=None)

# 设置字体，从而正确支持中文
plt.rcParams['font.family'] = ['SimHei']
plt.rcParams['font.sans-serif'] = ['SimHei']

# 只关注第6至第15列（索引从0开始，所以是5至14列）
subset_df = data.iloc[:, 5:15]

# 计算相关系数矩阵
corr_matrix = subset_df.corr()

# 创建并显示热力图
plt.figure(figsize=(12, 8))
sns.heatmap(corr_matrix, annot=True, cmap='coolwarm', linewidths=.5)
plt.title('Breast Cancer Wisconsin (Diagnostic) 数据集 - 第6至第15列特征相关性热力图')
plt.show()