import pandas as pd
import matplotlib.pyplot as plt

# 读取数据
data = pd.read_csv('./breast+cancer+wisconsin+diagnostic/wdbc.data', header=None)

# 设置字体，从而正确支持中文
plt.rcParams['font.family'] = ['SimHei']
plt.rcParams['font.sans-serif'] = ['SimHei']

# 提取第二列（索引从0开始，因此是第1列）
diagnosis = data[1]

# 统计良性（'B'）和恶性（'M'）样本的数量
benign_count = diagnosis.value_counts()['B']
malignant_count = diagnosis.value_counts()['M']

# 创建数据系列
labels = ['良性', '恶性']
counts = [benign_count, malignant_count]

# 绘制条形图
plt.figure(figsize=(8, 6))
plt.bar(labels, counts)
plt.xlabel('诊断结果分布')
plt.ylabel('数量')
plt.title('乳腺肿块诊断分布')
plt.xticks(rotation=45)

# 显示图形
plt.show()