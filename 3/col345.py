import pandas as pd
import matplotlib.pyplot as plt

# 读取数据
data = pd.read_csv('./breast+cancer+wisconsin+diagnostic/wdbc.data', header=None)

# 设置字体，从而正确支持中文
plt.rcParams['font.family'] = ['SimHei']
plt.rcParams['font.sans-serif'] = ['SimHei']

# 提取第3、3、5列的数据
column3 = data[2]
column4 = data[3]
column5 = data[4]

# 分别绘制直方图
fig, axs = plt.subplots(1, 3, figsize=(15, 5))

# 第3列直方图
axs[0].set_title('第3列特征分布')
axs[0].hist(column3, bins='auto',edgecolor='black')

# 第4列直方图
axs[1].set_title('第4列特征分布')
axs[1].hist(column4, bins='auto',edgecolor='black')

# 第5列直方图
axs[2].set_title('第5列特征分布')
axs[2].hist(column5, bins='auto',edgecolor='black')

# 显示图形
plt.tight_layout()
plt.show()